local PART = {}

PART.ID = "tardis2010_safelock4"
PART.Name = "2010 TARDIS Safelock 4"
PART.Model = "models/doctorwho1200/copper/safelock3.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/copper/safelock.wav"

TARDIS:AddPart(PART)