local PART = {}

PART.ID = "tardis2010_keyboard"
PART.Name = "2010 TARDIS Keyboard"
PART.Model = "models/doctorwho1200/copper/keyboard.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Sound = "vtalanov98/tardis2010/keyboard.wav"
PART.PowerOffSound = false
PART.PowerOffUse = true

TARDIS:AddPart(PART)