local PART = {}

PART.ID = "tardis2010_directionalpointer"
PART.Name = "2010 TARDIS Directional Pointer"
PART.Model = "models/doctorwho1200/copper/directionalpointer.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.7
PART.Sound = "doctorwho1200/copper/directionalpointer.wav"

TARDIS:AddPart(PART)