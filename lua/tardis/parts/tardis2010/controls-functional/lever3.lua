local PART = {}

PART.ID = "tardis2010_lever3"
PART.Name = "2010 TARDIS Lever 3 CLOAK TBA"
PART.Model = "models/doctorwho1200/copper/lever3.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3.5
PART.Sound = "doctorwho1200/copper/lever3.wav"

TARDIS:AddPart(PART)