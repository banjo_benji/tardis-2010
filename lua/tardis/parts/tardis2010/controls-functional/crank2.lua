local PART = {}

PART.ID = "tardis2010_crank2"
PART.Name = "2010 TARDIS Crank 2"
PART.Model = "models/doctorwho1200/copper/crank2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "doctorwho1200/copper/crank2.wav"

TARDIS:AddPart(PART)