local PART = {}

PART.ID = "tardis2010_throttle"
PART.Name = "2010 TARDIS Throttle"
PART.Model = "models/doctorwho1200/copper/throttle.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 2
PART.Sound = "doctorwho1200/copper/throttle.wav"

if SERVER then
	function PART:Use(ply)
		local power = self:GetData("power-state")
		local teleport = self:GetData("teleport", false)
		local vortex = self:GetData("vortex", false)
		local prefix = "models/doctorwho1200/copper/"

		if (teleport or vortex) then
			self.exterior:Mat()
		end
		if self.interior:GetSequencesEnabled() then return end
		if not (teleport or vortex) then
			self.exterior:Demat()
		end

		if power and self:GetOn() then
			throttle_texture = "throttlelightson"
		else
			throttle_texture = "throttlelightsoff"
		end

		self.interior:ChangeTexture("tardis2010_throttlelights", throttle_texture, nil, prefix)
	end
end

TARDIS:AddPart(PART)