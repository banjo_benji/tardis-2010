local PART = {}

PART.ID = "tardis2010_button2"
PART.Name = "2010 TARDIS Button 2"
PART.Model = "models/doctorwho1200/copper/button2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 3

PART.Sound = "doctorwho1200/copper/button.wav"
PART.PowerOffSound = false
PART.PowerOffUse = true

TARDIS:AddPart(PART)