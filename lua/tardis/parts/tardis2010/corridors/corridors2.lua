local PART = {}

PART.ID = "tardis2010_corridors2"
PART.Name = "2010 TARDIS Corridors 2"
PART.Model = "models/doctorwho1200/copper/corridors2.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)