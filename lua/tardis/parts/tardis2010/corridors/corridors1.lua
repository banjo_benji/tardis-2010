local PART = {}

PART.ID = "tardis2010_corridors1"
PART.Name = "2010 TARDIS Corridors 1"
PART.Model = "models/doctorwho1200/copper/corridors.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)