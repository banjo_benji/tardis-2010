local PART = {}

PART.ID = "tardis2010_telephone"
PART.Name = "2010 TARDIS Telephone"
PART.Model = "models/doctorwho1200/copper/telephone.mdl"
PART.AutoSetup = true

PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)