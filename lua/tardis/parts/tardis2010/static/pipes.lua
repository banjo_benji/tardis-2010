local PART = {}

PART.ID = "tardis2010_pipes"
PART.Name = "2010 TARDIS Pipes"
PART.Model = "models/doctorwho1200/copper/pipes.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)