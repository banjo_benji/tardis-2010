local PART = {}

PART.ID = "tardis2010_speedpad"
PART.Name = "2010 TARDIS Speed Pad"
PART.Model = "models/doctorwho1200/copper/speedpad.mdl"
PART.AutoSetup = true

PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)