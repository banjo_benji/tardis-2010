local PART = {}

PART.ID = "tardis2010_console"
PART.Name = "2010 TARDIS Console"
PART.Model = "models/doctorwho1200/copper/console.mdl"
PART.AutoSetup = true
PART.BypassIsomorphic = true

PART.Collision = true
PART.ShouldTakeDamage = true

TARDIS:AddPart(PART)