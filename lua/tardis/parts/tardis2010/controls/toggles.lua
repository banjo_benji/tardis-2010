local PART = {}

PART.ID = "tardis2010_toggles"
PART.Name = "2010 TARDIS Toggles"
PART.Model = "models/doctorwho1200/copper/toggles.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.1
PART.Sound = "vtalanov98/tardis2010/toggles.wav"

TARDIS:AddPart(PART)