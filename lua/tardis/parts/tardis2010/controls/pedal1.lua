local PART = {}

PART.ID = "tardis2010_pedal1"
PART.Name = "2010 TARDIS Pedal 1"
PART.Model = "models/doctorwho1200/copper/pedal.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.3
PART.Sound = "doctorwho1200/copper/pedal.wav"

TARDIS:AddPart(PART)