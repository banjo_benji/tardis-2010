local PART = {}

PART.ID = "tardis2010_steeringmech5"
PART.Name = "2010 TARDIS Steering Mechanism 5"
PART.Model = "models/doctorwho1200/copper/steeringmechanism5.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/copper/switch.wav"

TARDIS:AddPart(PART)