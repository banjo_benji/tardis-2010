local PART = {}

PART.ID = "tardis2010_stabilisers"
PART.Name = "2010 TARDIS Stabilisers"
PART.Model = "models/doctorwho1200/copper/boringer.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Sound = "vtalanov98/tardis2010/stabilizers.wav"

TARDIS:AddPart(PART)