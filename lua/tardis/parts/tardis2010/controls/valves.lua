local PART = {}

PART.ID = "tardis2010_valves"
PART.Name = "2010 TARDIS Hot and Cold Valves"
PART.Model = "models/doctorwho1200/copper/valves.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.7
PART.Sound = "vtalanov98/tardis2010/valves.wav"

TARDIS:AddPart(PART)