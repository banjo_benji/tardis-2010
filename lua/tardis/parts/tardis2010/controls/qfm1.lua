local PART = {}

PART.ID = "tardis2010_qfm1"
PART.Name = "2010 TARDIS Quantum Foam Manipulator 1"
PART.Model = "models/doctorwho1200/copper/quantumfoammanipulator.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 0.45
PART.Sound = "doctorwho1200/copper/quantumfoammanipulator.wav"

TARDIS:AddPart(PART)