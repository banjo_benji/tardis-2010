local PART = {}

PART.ID = "tardis2010_steeringmech4"
PART.Name = "2010 TARDIS Steering Mechanism 4"
PART.Model = "models/doctorwho1200/copper/steeringmechanism4.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 5
PART.Sound = "doctorwho1200/copper/switch.wav"

TARDIS:AddPart(PART)