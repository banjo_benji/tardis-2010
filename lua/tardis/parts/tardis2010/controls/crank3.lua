local PART = {}

PART.ID = "tardis2010_crank3"
PART.Name = "2010 TARDIS Crank 3"
PART.Model = "models/doctorwho1200/copper/crank3.mdl"
PART.AutoSetup = true

PART.Collision = true
PART.ShouldTakeDamage = true

PART.Animate = true
PART.AnimateSpeed = 1.0
PART.Sound = "doctorwho1200/copper/crank2.wav"

TARDIS:AddPart(PART)