TARDIS:AddControl({
	id = "tardis2010_animations",
	tip_text = nil,
	serveronly=true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		self:SetData("tardis2010_animations", not self:GetData("tardis2010_animations"), true)
	end,
})