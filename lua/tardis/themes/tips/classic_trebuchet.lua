local style = {
	style_id = "classic_trebuchet",
	style_name = "Classic Trebuchet",
	font = "Trebuchet24",
	colors = {
		normal = {
			text = Color(0, 0, 0, 255),
			background = Color(255, 255, 200, 255),
			frame = Color(0, 0, 0, 255),
		},
		highlighted = {
			text = Color(0, 0, 0),
			background = Color(138, 228, 111),
			frame = Color(0, 0, 0, 255),
		}
	}
}
TARDIS:AddTipStyle(style)