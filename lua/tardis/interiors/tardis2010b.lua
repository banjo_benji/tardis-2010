local T = {
	Base = "tardis2010",
	IsVersionOf = "tardis2010",
	ID = "tardis2010b",
}

T.Interior = {
	Light = { color = Color(255, 150, 50), pos = Vector(0, 0, 230), brightness = 5, warncolor = Color(255, 0, 0),},
	Lights = {
		{color = Color(255, 100, 0),		pos = Vector(-175.46, 73.716, 90.147),	brightness = 3, 	nopower = false, 	warncolor = Color(200, 0, 0),},
		{color = Color(255, 100, 0),		pos = Vector(139, -124, 130),			brightness = 3,		nopower = false, 	warncolor = Color(200, 2, 2),},
		{color = Color(0, 255, 230),		pos = Vector(0, 0, -60),				brightness = 0.6,	nopower = true, 	warncolor = Color(60, 0, 0),},
		{color = Color(0, 255, 230),		pos = Vector(0, 0, 40),					brightness = 0.2,	nopower = true, 	warncolor = Color(60, 0, 0),},
		{color = Color(0, 255, 230),		pos = Vector(337.3, -101.7, 58.4),		brightness = 0.5,	nopower = true, 	warncolor = Color(0, 0, 0),},
	},
}

T.Exterior = {}

TARDIS:AddInterior(T)