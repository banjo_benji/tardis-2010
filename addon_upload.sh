#! /bin/bash

filename=tardis_extension_tardis2010.gma
[[ -z $* ]] && echo "Changelog not specified! Aborting." && exit 1
[[ ! -f ./$filename ]] && echo "File $filename does not exist! Aborting." && exit 2

gmpublish update -id 590296269 -addon $filename -changes "$*"